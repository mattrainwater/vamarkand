﻿using System.Collections.Generic;
using System.Linq;

namespace Vamarkand.RulesEngine.State
{
    public class Field : GameObject
    {
        public List<BattleSquare> Squares { get; set; }

        public BattleSquare GetSquare(Position position)
        {
            return GetSquare(position.Row, position.Column);
        }

        public BattleSquare GetSquare(Row row, Column col)
        {
            return Squares.First(x => x.Position.Row == row && x.Position.Column == col);
        }
    }
}