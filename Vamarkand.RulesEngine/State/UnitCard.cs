﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.Action;
using Vamarkand.RulesEngine.Effect;

namespace Vamarkand.RulesEngine.State
{
    public class UnitCard : Card
    {
        public int Health { get; set; }
        public int Attack { get; set; }
        public Dictionary<Column, GameAction> Abilities { get; set; }
        public Dictionary<Column, GameEffect> PersistentEffects { get; set; }
    }
}
