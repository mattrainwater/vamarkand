﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.Effect;

namespace Vamarkand.RulesEngine.State
{
    public class Unit : GameObject
    {
        public Unit(UnitCard unitCard)
        {
            UnitCard = unitCard;

            MaxHealth = unitCard.Health;
            CurrentHealth = unitCard.Health;
            DefaultHealth = unitCard.Health;

            CurrentAttack = unitCard.Attack;
            DefaultAttack = unitCard.Attack;
        }

        public int MaxHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int DefaultHealth { get; set; }
        public int CurrentAttack { get; set; }
        public int DefaultAttack { get; set; }

        public UnitCard UnitCard { get; set; }

        public List<GameEffect> PersistentEffects { get; set; }
    }
}
