﻿namespace Vamarkand.RulesEngine.State
{
    public enum Phase
    {
        Upkeep,
        Main,
        End
    }
}