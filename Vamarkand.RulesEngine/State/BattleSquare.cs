﻿namespace Vamarkand.RulesEngine.State
{
    public class BattleSquare : GameObject
    {
        public Unit Unit { get; set; }
        public Position Position { get; set; }
    }
}