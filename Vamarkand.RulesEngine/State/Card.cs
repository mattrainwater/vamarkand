﻿namespace Vamarkand.RulesEngine.State
{
    public abstract class Card : GameObject
    {
        public int ManaCost { get; set; }
        public string Name { get; set; }
    }
}