﻿namespace Vamarkand.RulesEngine.State
{
    public class Position : GameObject
    {
        public Column Column { get; set; }
        public Row Row { get; set; }
    }
}