﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vamarkand.RulesEngine.State
{
    public class GameState
    {
        public Player HumanPlayer { get; set; }
        public Player EnemyPlayer { get; set; }

        public Player CurrentPlayer { get; set; }

        public Phase CurrentPhase { get; set; }
    }
}
