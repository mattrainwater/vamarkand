﻿using System.Collections.Generic;
using Vamarkand.RulesEngine.Effect;

namespace Vamarkand.RulesEngine.State
{
    public class Player : GameObject
    {
        public List<Card> Hand { get; set; }
        public List<Card> Discard { get; set; }
        public List<Card> Deck { get; set; }

        public int CurrentMana { get; set; }
        public int MaxMana { get; set; }

        public Field BattleField { get; set; }

        public List<GameEffect> PersistentEffects { get; set; }
    }
}