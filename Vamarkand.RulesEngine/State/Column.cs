﻿namespace Vamarkand.RulesEngine.State
{
    public enum Column
    {
        Vanguard,
        Flank,
        Rear
    }
}