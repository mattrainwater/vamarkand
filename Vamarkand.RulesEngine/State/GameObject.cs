﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vamarkand.RulesEngine.State
{
    public abstract class GameObject
    {
        private Guid _id;
        public Guid Id
        {
            get
            {
                if(_id == Guid.Empty)
                {
                    _id = Guid.NewGuid();
                }
                return _id;
            }
        }
    }
}
