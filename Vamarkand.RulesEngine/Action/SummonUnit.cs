﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.RulesEngine.Action
{
    public class SummonUnit : GameAction
    {
        public const string RETURN_UNIT = "unit";
        public const string RETURN_POSITION = "position";
        public const string RETURN_MANACOST = "manacost";
        public const string RETURN_PLAYER = "player";
        public const string RETURN_CURRENT_MANA = "currentMana";

        public const string SUMMON_UNIT_NAME = "summonunit";

        private Position _position;
        private UnitCard _unitCard;
        private Player _player;

        public SummonUnit(Position position, UnitCard unitCard, Player player)
        {
            _position = position;
            _unitCard = unitCard;
            _player = player;
        }

        public override Dictionary<string, object> Perform(GameState gameState)
        {
            // remove card from hand
            _player.Hand.Remove(_unitCard);

            // make a unit and put it on the position
            var unit = new Unit(_unitCard);
            var square = _player.BattleField.GetSquare(_position);
            square.Unit = unit;

            // reduce mana by its cost
            _player.CurrentMana -= _unitCard.ManaCost;

            // return result
            return new Dictionary<string, object>
            {
                { RETURN_UNIT, unit },
                { RETURN_POSITION, _position },
                { RETURN_MANACOST, _unitCard.ManaCost },
                { RETURN_CURRENT_MANA, _player.CurrentMana },
                { RETURN_PLAYER, _player },
                { GameRulesEngine.ACTION_PERFORMED_LABEL, SUMMON_UNIT_NAME },
            };
        }
    }
}
