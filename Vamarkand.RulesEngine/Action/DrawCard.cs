﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.RulesEngine.Action
{
    public class DrawCard : GameAction
    {
        public const string RETURN_PLAYER = "player";
        public const string RETURN_CARD = "card";

        public const string DRAW_EVENT_NAME = "drawcard";

        private Player _player;

        public DrawCard(Player player)
        {
            _player = player;
        }

        public override Dictionary<string, object> Perform(GameState gameState)
        {
            var cardToDraw = _player.Deck.First();

            _player.Deck.Remove(cardToDraw);
            _player.Hand.Add(cardToDraw);

            return new Dictionary<string, object> {
                { RETURN_CARD, cardToDraw },
                { RETURN_PLAYER, _player },
                { GameRulesEngine.ACTION_PERFORMED_LABEL, DRAW_EVENT_NAME }
            };
        }
    }
}
