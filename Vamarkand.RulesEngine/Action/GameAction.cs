﻿using System.Collections.Generic;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.RulesEngine.Action
{
    public abstract class GameAction
    {
        public abstract Dictionary<string, object> Perform(GameState gameState);
    }
}