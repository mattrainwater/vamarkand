﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.RulesEngine.Action
{
    public class AttackWithUnit : GameAction
    {
        public const string RETURN_TARGET = "target";
        public const string RETURN_TARGET_DAMAGE = "targetDamage";
        public const string RETURN_SOURCE = "source";
        public const string RETURN_SOURCE_DAMAGE = "sourceDamage";

        public const string ATTACK_UNIT_NAME = "attackunit";

        private Unit _source;
        private Unit _target;

        public AttackWithUnit(Unit source, Unit target)
        {
            _source = source;
            _target = target;
        }

        public override Dictionary<string, object> Perform(GameState gameState)
        {
            // subtract source attack from target health
            _target.CurrentHealth -= _source.CurrentAttack;
            _source.CurrentHealth -= _target.CurrentAttack;
            var result = new Dictionary<string, object> {
                { RETURN_TARGET, _target.Id },
                { RETURN_SOURCE, _source.Id },
                { RETURN_SOURCE_DAMAGE, _source.CurrentAttack },
                { RETURN_TARGET_DAMAGE, _target.CurrentAttack },
                { GameRulesEngine.ACTION_PERFORMED_LABEL, ATTACK_UNIT_NAME },
            };

            return result;
        }
    }
}
