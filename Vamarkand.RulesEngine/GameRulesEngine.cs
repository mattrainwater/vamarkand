﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.Action;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.RulesEngine
{
    public class GameRulesEngine
    {
        public const string ACTION_PERFORMED_LABEL = "action-performed";
        public const string EMPTY_ACTION = "empty";

        private List<GameAction> _queuedActions;
        private readonly Dictionary<string, object> EMPTY_RESULT = new Dictionary<string, object> {
            { ACTION_PERFORMED_LABEL, EMPTY_ACTION }
        };

        public GameRulesEngine()
        {
            _queuedActions = new List<GameAction>();
            _queuedActions.Add(new StartGame());
        }

        public void QueueAction(GameAction action)
        {
            _queuedActions.Add(action);
        }

        public Dictionary<string, object> Execute(GameState state)
        {
            if(HasQueuedActions())
            {
                var diff = _queuedActions.First().Perform(state);
                _queuedActions.RemoveAt(0);
                CheckStateBasedActions(state);
                return diff;
            }
            return EMPTY_RESULT;
        }

        public List<GameAction> GetAvailableActions(GameState state)
        {
            return null;
        }

        public bool HasQueuedActions()
        {
            return _queuedActions.Any();
        }

        private void CheckStateBasedActions(GameState state)
        {
            foreach(var unit in state.CurrentPlayer.BattleField.Squares.Select(x => x.Unit))
            {
                if(unit.CurrentHealth <= 0)
                {
                    QueueAction(new UnitKilled(unit));
                }
            }
            foreach (var unit in state.EnemyPlayer.BattleField.Squares.Select(x => x.Unit))
            {
                if (unit.CurrentHealth <= 0)
                {
                    QueueAction(new UnitKilled(unit));
                }
            }
        }
    }
}
