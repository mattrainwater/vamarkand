﻿using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.Common;

namespace Vamarkand.Scenes
{
    public class BaseScene : Scene
    {
        public const int SCREEN_SPACE_RENDER_LAYER = 999;

        public BaseScene()
        {
            addRenderer(new RenderLayerExcludeRenderer(0, SCREEN_SPACE_RENDER_LAYER));
            addRenderer(new ScreenSpaceRenderer(100, SCREEN_SPACE_RENDER_LAYER));
        }

        public override void initialize()
        {
            setDesignResolution(DimensionConstants.DESIGN_WIDTH, DimensionConstants.DESIGN_HEIGHT, SceneResolutionPolicy.ShowAllPixelPerfect);
            clearColor = Color.Green;
        }
    }
}
