﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Tiled;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.Battle;
using Vamarkand.Common;
using Vamarkand.RulesEngine.Effect;
using Vamarkand.RulesEngine.State;

namespace Vamarkand.Scenes
{
    public class TestCardBattleScene : BaseScene
    {
        public TestCardBattleScene()
            : base()
        {
        }

        public override void onStart()
        {
            var map = content.Load<TiledMap>("battle");
            var entity = createEntity("map");
            var tiledMapComponent = entity.addComponent(new TiledMapComponent(map));
            tiledMapComponent.renderLayer = 1000;
            map.layers[0].scale = 2f;
            map.layers[1].scale = 2f;
            map.layers[2].scale = 2f;

            var gameState = new GameState();
            gameState.CurrentPhase = Phase.Upkeep;
            var humanPlayer = GetPlayer();
            gameState.HumanPlayer = humanPlayer;
            gameState.EnemyPlayer = GetPlayer();
            gameState.CurrentPlayer = humanPlayer;

            var battleController = new BattleController(gameState);
            var controllerEntity = createEntity("controller");
            controllerEntity.addComponent(battleController);
        }

        private Player GetPlayer()
        {
            return new Player
            {
                BattleField = GetEmptyBattleField(),
                CurrentMana = 0,
                Deck = GetDeck(),
                Discard = new List<Card>(),
                Hand = new List<Card>(),
                MaxMana = 0,
                PersistentEffects = new List<GameEffect>()
            };
        }

        private List<Card> GetDeck()
        {
            var deck = new List<Card>();

            for(var i = 0; i < 15; i++)
            {
                deck.Add(new UnitCard()
                {
                    Abilities = new Dictionary<Column, RulesEngine.Action.GameAction>(),
                    Attack = 1,
                    Health = 1,
                    ManaCost = 1,
                    Name = "",
                    PersistentEffects = new Dictionary<Column, GameEffect>()
                });
            }

            return deck;
        }

        private Field GetEmptyBattleField()
        {
            return new Field
            {
                Squares = new List<BattleSquare>
                {
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Bottom,
                            Column = Column.Rear
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Bottom,
                            Column = Column.Flank
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Bottom,
                            Column = Column.Vanguard
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Middle,
                            Column = Column.Rear
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Middle,
                            Column = Column.Flank
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Middle,
                            Column = Column.Vanguard
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Top,
                            Column = Column.Rear
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Top,
                            Column = Column.Flank
                        }
                    },
                    new BattleSquare
                    {
                        Position = new Position
                        {
                            Row = Row.Top,
                            Column = Column.Vanguard
                        }
                    }
                }
            };
        }
    }
}
