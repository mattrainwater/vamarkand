﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using System.Collections.Generic;
using Vamarkand.Common;
using Vamarkand.Scenes;

namespace Vamarkand
{
    public class GameMain : Core
    {
        public GameMain()
            : base(width: DimensionConstants.SCREEN_WIDTH, height: DimensionConstants.SCREEN_HEIGHT, isFullScreen: DimensionConstants.IS_FULL_SCREEN, windowTitle: "Vamarkand")
        {
            defaultSamplerState = SamplerState.PointClamp;
        }

        protected override void Initialize()
        {
            base.Initialize();
            //GlobalState.Characters = LoadCharacters();
            //var unitTexture = content.Load<Texture2D>("units");
            //var unitSubtextures = Subtexture.subtexturesFromAtlas(unitTexture, 32, 32);
            //GlobalState.UnitSprites = new TextureAtlas(GetUnitNames(), unitSubtextures.ToArray());
            //CommonResources.CardBack = content.Load<Texture2D>("card");
            //CommonResources.CardFront = content.Load<Texture2D>("card_front");
            //CommonResources.DefaultBitmapFont = Graphics.instance.bitmapFont;
            //CommonResources.Pixel = content.Load<Texture2D>("pixel");
            //VirtualButtons.SetupInput();
            //Time.altTimeScale = 3.75f;
            scene = new TestCardBattleScene();
        }

        //private string[] GetUnitNames()
        //{
        //    return new List<string>() {
        //        "paladin","spearfighter","axeman", "inquisitor", "alchemist", "herald",
        //        "swordsman","landsknecht","gladiator", "hermit", "firestarter", "cryomancer",
        //        "arcanist","assassin","astrologist", "druid", "thief", "spellsword",
        //        "hunter","necromancer","bugler", "shogun", "brigand", "ninja",
        //        "mystic","gunner","witch", "reaper", "seacaller", "outrunner",
        //        "highlander","harpoonist","sailor", "monk", "hammerer", "duelist",
        //    }.ToArray();
        //}

        //private List<CharacterState> LoadCharacters()
        //{
        //    var characterText = File.ReadAllText("Content/characters.dat");
        //    return JsonConvert.DeserializeObject<List<CharacterState>>(characterText, new JsonSerializerSettings
        //    {
        //        TypeNameHandling = TypeNameHandling.All
        //    });
        //}
    }
}
