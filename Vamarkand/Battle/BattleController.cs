﻿using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vamarkand.RulesEngine.State;
using Vamarkand.RulesEngine;
using Vamarkand.RulesEngine.Action;

namespace Vamarkand.Battle
{
    public class BattleController : Component, IUpdatable
    {
        public bool Paused { get; set; }
        public bool Ready { get; set; }

        private GameState _gameState;
        private GameRulesEngine _rulesEngine;

        private GameAction _nextAction;

        public BattleController(GameState gameState)
        {
            _gameState = gameState;
            _rulesEngine = new GameRulesEngine();
            Ready = true;
        }

        public void update()
        {
            if (!Paused)
            {
                if(_rulesEngine.HasQueuedActions() && Ready)
                {
                    var result = _rulesEngine.Execute(_gameState);
                    GenerateUIChanges(result);
                    GenerateActions(_rulesEngine.GetAvailableActions(_gameState));
                }
                else if(_nextAction != null && Ready)
                {
                    _rulesEngine.QueueAction(_nextAction);
                    _nextAction = null;
                }
            }
        }

        private void GenerateActions(List<GameAction> list)
        {
        }

        private void GenerateUIChanges(Dictionary<string, object> result)
        {
        }
    }
}
